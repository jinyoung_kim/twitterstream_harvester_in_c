#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <oauth.h>
#include <curl/curl.h>

#include <jansson.h>

char *global_buffer = NULL;
int buffer_size = 0;

json_t *is_geotagged(json_t *obj) {
	json_t *geo_obj = json_object_get(obj, "geo");
       	json_t *geo_obj_coordinates = json_object_get(geo_obj, "coordinates");  
        if(geo_obj_coordinates != NULL) {
        	return obj;
        } else {
       		return NULL;
	}
}

size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata) {
//	printf("%s\n", ptr);
		
	char *contents = NULL;

	if(global_buffer != NULL) {
		int new_buffer_size = nmemb*size + buffer_size + 1;
		char *new_global_buffer = (char *) realloc(global_buffer, new_buffer_size);
		if(new_global_buffer != NULL) {
			//free(global_buffer);
			global_buffer = new_global_buffer;
			strncat(global_buffer, ptr, nmemb);
			buffer_size = new_buffer_size;

			contents = global_buffer;
			
		} else {
			printf("Realloc Error!\n");
			// give up retreving the tweet if the allocation fails..
			if(global_buffer != NULL)
				free(global_buffer);

			global_buffer = NULL;
			buffer_size = 0;
		}
		//contents = global_buffer;
	} else {
		contents = ptr;
	}

	json_error_t error;
	json_t *obj = json_loads(contents, 0, &error);
	if(obj == NULL) {
		printf("Error! : line: %d, %s\n\n", error.line, error.text);
		if(global_buffer == NULL) {
			global_buffer = (char *) calloc (nmemb+1, size);
			strncpy(global_buffer, contents, nmemb);
			buffer_size = nmemb * size;
		}
		printf("size*nmemb = %d, buffer_size = %d\n", (int)size*(int)nmemb, buffer_size);
	} else {
		if(is_geotagged(obj)) {
			printf("%s\n", contents);
		}
		/**
		json_t *geo_obj = json_object_get(obj, "geo");
		json_t *geo_obj_coordinates = json_object_get(geo_obj, "coordinates");
		if(geo_obj_coordinates != NULL) {
		
			const char *key;
	                json_t *value;
        	        void *iter = json_object_iter(geo_obj);
			while(iter) {
				key = json_object_iter_key(iter);
				value = json_object_iter_value(iter);
				printf("%s : %s\n", key, json_string_value(value));

                        	iter = json_object_iter_next(geo_obj, iter);
			}
			
			printf("%s\n", contents);
		} else {
			//printf("%s\n", contents);
		}*/
		if(global_buffer != NULL) {
			free(global_buffer);
			global_buffer = NULL;
			buffer_size = 0;
		}	
	}
	// size == sizeof(-)
	// nmemb == # of memory blocks
	//printf("%d, %d\n", size, nmemb);
	
	return size*nmemb;
	//return fwrite(contents, size, nmemb, stdout);
}

int main(int argc, const char *argv[]) {
	/**
	FILE *out;
	if(argc == 2) {
		out = fopen(argv[1], "w");
	} else if (argc == 1) {
		out = stdout;
	} else {
		printf("usage: %s [outfile]\n", argv[0]);
		return 1;
	}*/

	const char *ckey = "729aMJNWhduz3A9rbKurcuBbg";
	const char *csecret = "vHnhpiXiBrcIUrB9V59fefnU3u1Upl8o0mUqUzk9inJFVBVdMJ";
	const char *atok = "1253120136-aMrV0HnvVIZWuFfP28fMTX31yoj1ik1nSa2gPJN";
	const char *atoksecret = "KuHJmujQY1TGfWD7ebp7qfgCIqlPlCzIuPruEpoFYASC6";
	
	curl_global_init(CURL_GLOBAL_ALL);
	CURL *curl = curl_easy_init();

	// Bounding-box (Greater Melbourne) : 144.33361129600002, -38.502988015499966, 145.878412, -37.17509899299998
	// Retrieved from AURIN
	//const char *url = "https://stream.twitter.com/1.1/statuses/filter.json?locations=144.884643713,-37.892242103,145.051636706,-37.72702566";
	const char *url = "https://stream.twitter.com/1.1/statuses/filter.json?locations=144.33361129600002,-38.502988015499966,145.878412,-37.17509899299998";	
	// URL, POST parameters (not used in this example), OAuth signing method, HTTP method, keys
	char *signedurl = oauth_sign_url2(url, NULL, OA_HMAC, "GET", ckey, csecret, atok, atoksecret);
	
	// URL we're connecting to
    	curl_easy_setopt(curl, CURLOPT_URL, signedurl);
    
    	// User agent we're going to use, fill this in appropriately
    	curl_easy_setopt(curl, CURLOPT_USERAGENT, "appname/0.1");
    
    	// libcurl will now fail on an HTTP error (>=400)
    	curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
    
   	// In this case, we're not specifying a callback function for
   	// handling received data, so libcURL will use the default, which
   	// is to write to the file specified in WRITEDATA
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	//curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)data);    
	
	// Execute the request!
    	int curlstatus = curl_easy_perform(curl);
    	printf("curl_easy_perform terminated with status code %d\n", curlstatus);
    
    	curl_easy_cleanup(curl);
    	curl_global_cleanup();
    	//fclose(out);

	return 0;
}
