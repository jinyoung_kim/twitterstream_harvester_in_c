#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <oauth.h>
#include <curl/curl.h>

#include <jansson.h>

char *global_buffer = NULL;
int buffer_size = 0;

static char *DB_ADDRESS = "http://115.146.95.150:5984/streaming_brisbane";
static char *CONTENT_TYPE_HEADER = "Content-Type: application/json";

void curl_post(char *json_str) {
	CURL *curl = curl_easy_init();
	if(curl) {
		struct curl_slist *header = NULL;
		header = curl_slist_append(header, CONTENT_TYPE_HEADER);
		if(header) {
			curl_easy_setopt(curl, CURLOPT_URL, DB_ADDRESS);
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header);
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_str);
			curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(json_str));
			
			int curlstatus = curl_easy_perform(curl);
        		printf("curl_post: curl_easy_perform terminated with status code %d\n", curlstatus);
        		curl_slist_free_all(header);
		}
		curl_easy_cleanup(curl);
	}
}

json_t *is_geotagged(json_t *obj) {
	json_t *geo_obj = json_object_get(obj, "geo");
       	json_t *geo_obj_coordinates = json_object_get(geo_obj, "coordinates");  
        if(geo_obj_coordinates) 
        	return obj;
        else 
       		return NULL;
}

json_t *add_id_field(json_t *obj) {
	json_t *id_str = json_object_get(obj, "id_str");
	if(id_str) {
		json_object_set_new(obj, "_id", id_str);
		return obj;
	}
	else
		return NULL;	
}

size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata) {
		
	char *contents = NULL;

	if(global_buffer) {
		int new_buffer_size = nmemb*size + buffer_size + 1;
		char *new_global_buffer = (char *) realloc(global_buffer, new_buffer_size);
		if(new_global_buffer) {
			//free(global_buffer);
			global_buffer = new_global_buffer;
			strncat(global_buffer, ptr, nmemb);
			buffer_size = new_buffer_size;

			contents = global_buffer;
			
		} else {
			printf("Realloc Error!\n");
			// give up retreving the tweet if the allocation fails..
			if(global_buffer != NULL)
				free(global_buffer);

			global_buffer = NULL;
			buffer_size = 0;
		}
	} else {
		contents = ptr;
	}

	json_error_t error;
	json_t *obj = json_loads(contents, 0, &error);
	if(obj == NULL) {
		//printf("Error! : line: %d, %s\n\n", error.line, error.text);
		if(global_buffer == NULL) {
			global_buffer = (char *) calloc (nmemb+1, size);
			strncpy(global_buffer, contents, nmemb);
			buffer_size = nmemb * size;
		}
		//printf("size*nmemb = %d, buffer_size = %d\n", (int)size*(int)nmemb, buffer_size);
	} else {
		if(is_geotagged(obj)) {
			//printf("%s\n", contents);
			char *new_json_str = json_dumps(add_id_field(obj), 0);
			//puts(new_json_str);
			curl_post(new_json_str);
		}
		if(global_buffer != NULL) {
			free(global_buffer);
			global_buffer = NULL;
			buffer_size = 0;
		}	
		json_decref(obj);
	}
	// size == sizeof(-)
	// nmemb == # of memory blocks
	//printf("%d, %d\n", size, nmemb);
	
	return size*nmemb;
	//return fwrite(contents, size, nmemb, stdout);
}

int main(int argc, const char *argv[]) {

	const char *ckey = "Qv5nTBDgBgmyeu2x5yZQn45R1";
	const char *csecret = "dvVYcAwi5QJmDxPJNrrpXIWcWm4HfpbQ3CcGw2bTs8LLy6Kt9Z";
	const char *atok = "1675306255-bFZRqWtpAwnqZbmBcsGRszy7mmm1KbC9zymJdmF";
	const char *atoksecret = "nPljatlXPbW7gkgvNWjjUNf7gdbhUQ6aqfG7WC7WynHEH";
	
    while (1) {
        curl_global_init(CURL_GLOBAL_ALL);
        CURL *curl = curl_easy_init();
        
        // Bounding-box (Greater Brisbane) : 152.0734, -28.3640, 153.5467, -26.4519
        // Retrieved from AURIN
        const char *url = "https://stream.twitter.com/1.1/statuses/filter.json?locations=152.0734,-28.3640,153.5467,-26.4519";
        // URL, POST parameters (not used in this example), OAuth signing method, HTTP method, keys
        char *signedurl = oauth_sign_url2(url, NULL, OA_HMAC, "GET", ckey, csecret, atok, atoksecret);
        
        // URL we're connecting to
        curl_easy_setopt(curl, CURLOPT_URL, signedurl);
        
        // User agent we're going to use, fill this in appropriately
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "appname/0.1");
        
        // libcurl will now fail on an HTTP error (>=400)
        curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
        
        // In this case, we're not specifying a callback function for
        // handling received data, so libcURL will use the default, which
        // is to write to the file specified in WRITEDATA
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
        //curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)data);
        
        // Execute the request!
        int curlstatus = curl_easy_perform(curl);
        printf("main: curl_easy_perform terminated with status code %d\n", curlstatus);
        
        curl_easy_cleanup(curl);
        curl_global_cleanup();
        //fclose(out);
    }

	return 0;
}
