CC = gcc
CFLAGS = -std=c99
CLIBS = -lcurl -loauth -ljansson

main1 = harvester_melbourne
main2 = harvester_perth
main3 = harvester_sydney
main4 = harvester_brisbane

OBJPATH = ./src/harvester
main1_obj = $(OBJPATH)/harvester_melbourne.o
main2_obj = $(OBJPATH)/harvester_perth.o
main3_obj = $(OBJPATH)/harvester_sydney.o
main4_obj = $(OBJPATH)/harvester_brisbane.o


module1 : $(main1_obj)
	$(CC) $(CFLAGS) -o $(main1) $(main1_obj) $(CLIBS)
module2 : $(main2_obj)
	$(CC) $(CFLAGS) -o $(main2) $(main2_obj) $(CLIBS)
module3 : $(main3_obj)
	$(CC) $(CFLAGS) -o $(main3) $(main3_obj) $(CLIBS)
module4 : $(main4_obj)
	$(CC) $(CFLAGS) -o $(main4) $(main4_obj) $(CLIBS)

install:
	make module1
	make module2
	make module3
	make module4
	make clean

clean:
	rm -f $(main1_obj)
	rm -f $(main2_obj)
	rm -f $(main3_obj)
	rm -f $(main4_obj)

uninstall:
	make clean
	rm -f $(main1)
	rm -f $(main2)
	rm -f $(main3)
	rm -f $(main4)
